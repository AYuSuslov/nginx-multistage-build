root@deb1:/home/suslov/less4# docker build -t nginx_my:v2 -f Dockerfile_my_m .
Sending build context to Docker daemon  61.95kB
Step 1/8 : FROM debian:9 as build
 ---> b33ba41eae78
Step 2/8 : RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev
 ---> Using cache
 ---> fafd36df945d
Step 3/8 : RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure && make && make install
 ---> Using cache
 ---> 9fbc4be017b0
Step 4/8 : FROM debian:9
 ---> b33ba41eae78
Step 5/8 : WORKDIR /usr/local/nginx/sbin
 ---> Running in cbe59f436740
Removing intermediate container cbe59f436740
 ---> f317245813a0
Step 6/8 : COPY --from=build /usr/local/nginx/sbin/nginx .
 ---> a04aa6d62842
Step 7/8 : RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
 ---> Running in 472661689cbc
Removing intermediate container 472661689cbc
 ---> 11af1119ceb7
Step 8/8 : CMD ["./nginx", "-g", "daemon off;"]
 ---> Running in 292d4e1de8d4
Removing intermediate container 292d4e1de8d4
 ---> 0aeac658fdb1
Successfully built 0aeac658fdb1
Successfully tagged nginx_my:v2
root@deb1:/home/suslov/less4# 
root@deb1:/home/suslov/less4# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nginx_my            v2                  0aeac658fdb1        12 seconds ago      108MB
nginx_my            v1                  63d6dd824eca        3 hours ago         274MB
nginxsc             v1                  cdb8471abb2a        7 hours ago         269MB
nginxsc             v2                  d06901f0c872        13 hours ago        107MB
less4test           v1                  7e13b0851836        2 weeks ago         173MB
nginx               latest              bc9a0695f571        3 weeks ago         133MB
debian              9                   b33ba41eae78        4 weeks ago         101MB
hello-world         latest              bf756fb1ae65        11 months ago       13.3kB
root@deb1:/home/suslov/less4# docker run -v $(pwd)/nginx.conf:/usr/local/nginx/conf/nginx.conf --name nginx_my_less4_v2 -d -p 8088:80 nginx_my:v2
9e0eea4f0ff94e1aaf83549bbbdddb0e80652c9834d064b830f83a226da0827b
root@deb1:/home/suslov/less4# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
9e0eea4f0ff9        nginx_my:v2         "./nginx -g 'daemon …"   5 seconds ago       Up 4 seconds        0.0.0.0:8088->80/tcp   nginx_my_less4_v2
16d6097d0cd9        nginxsc:v1          "/usr/local/nginx/sb…"   7 hours ago         Up 7 hours          0.0.0.0:8005->80/tcp   agitated_goldwasser
d899c6cf468e        nginx:latest        "/docker-entrypoint.…"   2 weeks ago         Up 2 weeks          0.0.0.0:8004->80/tcp   nginx_less4_v2
4b85ee41e27c        nginx:latest        "/docker-entrypoint.…"   2 weeks ago         Up 2 weeks          0.0.0.0:8003->80/tcp   frosty_elbakyan
e4655fa27dcf        nginx:latest        "/docker-entrypoint.…"   2 weeks ago         Up 2 weeks          0.0.0.0:8002->80/tcp   sweet_goldstine
root@deb1:/home/suslov/less4# curl localhost:8088
<html>
<head><title>404 Not Found</title></head>
<body bgcolor="white">
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.0.5</center>
</body>
</html>
root@deb1:/home/suslov/less4#