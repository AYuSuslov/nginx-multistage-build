FROM debian:9 as build

RUN apt update && apt install -y wget gcc make openssl libpcre3 libpcre3-dev zlib1g zlib1g-dev
RUN mkdir /tmp/nginx_lua_build && cd /tmp/nginx_lua_build && \
	wget http://nginx.org/download/nginx-1.18.0.tar.gz && \
	wget -O LuaJIT-2.1.tar.gz https://github.com/openresty/luajit2/archive/v2.1-20201027.tar.gz && \
	wget -O nginx_devel_kit.tar.gz https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz && \
	wget -O lua_nginx_module.tar.gz https://github.com/openresty/lua-nginx-module/archive/v0.10.19.tar.gz && \
	wget -O lua_resty_lrucache.tar.gz https://github.com/openresty/lua-resty-lrucache/archive/v0.10.tar.gz && \
	wget -O lua_resty_core.tar.gz https://github.com/openresty/lua-resty-core/archive/v0.1.21.tar.gz && \
	tar xvfz nginx-1.18.0.tar.gz && \
	tar xvfz LuaJIT-2.1.tar.gz && \
	tar xvfz nginx_devel_kit.tar.gz && \
	tar xvfz lua_nginx_module.tar.gz && \
	tar xvfz lua_resty_lrucache.tar.gz && \
	tar xvfz lua_resty_core.tar.gz && \
	cd /tmp/nginx_lua_build/luajit2-2.1-20201027 && make && make install && \
	cd /tmp/nginx_lua_build/lua-resty-lrucache-0.10 && make && make install && \
	cd /tmp/nginx_lua_build/lua-resty-core-0.1.21 && make && make install
RUN	export LUAJIT_LIB=/usr/local/lib && \
	export LUAJIT_INC=/usr/local/include/luajit-2.1 && \
	cd /tmp/nginx_lua_build/nginx-1.18.0 && \
	./configure \
    --prefix=/usr/local/nginx              \
    --sbin-path=/usr/local/nginx/sbin/nginx \
    --conf-path=/usr/local/nginx/conf/nginx.conf \
    --pid-path=/usr/local/nginx/logs/nginx.pid \
    --error-log-path=/usr/local/nginx/logs/error.log \
    --http-log-path=/usr/local/nginx/logs/access.log \
    --without-http_scgi_module            \
    --without-http_uwsgi_module           \
    --without-http_fastcgi_module ${NGINX_DEBUG:+--debug} \
    --with-cc-opt=-O2 --with-ld-opt='-Wl,-rpath,/usr/local/lib' \
    --add-module=/tmp/nginx_lua_build/ngx_devel_kit-0.3.1 \
    --add-module=/tmp/nginx_lua_build/lua-nginx-module-0.10.19 && \
	make  -j2 && make install
	
FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/local/lib/libluajit-5.1.so.2.1.0 /usr/local/lib/
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
COPY --from=build /usr/local/nginx/conf/mime.types /usr/local/nginx/conf
COPY --from=build /usr/local/lib/lua /usr/local/lib/lua 
CMD ["./nginx", "-g", "daemon off;"]
